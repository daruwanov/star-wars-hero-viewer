# Star Wars Hero Viewer #

Simple chrome extension for navigate throw star wars heroes

### Install ###

* Cloning this repository `git clone git@bitbucket.org:daruwanov/star-wars-hero-viewer.git`
* Enter to project work directory `cd /path/to/project/work/directory`
* Be sure that you have installed:
    * `node` : [NODEJS](https://nodejs.org/en/)
    * `npm` : [NPM](https://www.npmjs.com)
    * `bower` : `npm install -g bower`
    * `grunt-cli` : `npm install -g grunt-cli`
    * `browserify`: `npm install -g browserify`
* Run next scripts `npm install && bower install`
* Run `grunt starwars` or just `grunt`(will run default task);
    * This will automatically build project:
        * convert `scss` to `css`
        * start project in browser (localhost:3000)
        * run `watch` task to refresh browser on any changes in project files
* For more comfortable viewing while development open page in responsive mode  (450X350px)


### Libraries and Technologies ###

* AngularJs - Main Framework
* AngularRoute - Routing in App
* ne-swapi - Angular library for making requests to swapi.co site. (getting from swapi.co)
* lodash - Just Awesome `:)`
* bootstrap - grid styles
* bower, npm, grunt

### Used grunt modules ###

* `grunt-browserify` - run local server and start browser
* `grunt-contrib-watch` - watch for file changes
* `grunt-sass` - convert `.sass` to `.css`
* `load-grunt-tasks` - for simple loading grunt task in gruntfile
* `grunt-contrib-clean` - clean dist directory
* `grunt-contrib-copy` - copy source files
* `grunt-text-replace` - replace text in files

### Assets ###

* Font `Star Jedi Outline`
* 2 files of mouse cursors (red and green Sword of the Jedi) PS: green only on links

### Enviroments ###

* Development - Work with `grunt starwars` task, and can run in browser. To load development version as extension load project directiry
* Production - Work with `grunt build` - build project in `dist` directory. And replace manifest file for new filepath.

* Also exist `build_local` task - its build development version - without running watch task.

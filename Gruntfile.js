(function () {
  'use strict';
  module.exports = exportFunction;

  function exportFunction(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
      sass: {
        options: {sourceMap: true},
        dist: {
          files: {
            'src/styles/app.css': 'src/styles/app.scss'
          }
        }
      },
      clean: {
        dist: {
          files: [
            {
              dot: true,
              src: ['dist/*']
            }
          ]
        }
      },
      copy: {
        dist: {
          files: [{
            expand: true,
            dot: true,
            cwd: 'src/bower_components',
            src: ['angular/angular.min.js', 'angular-route/angular-route.min.js', 'lodash/lodash.js', 'ne-swapi/dist/ne-swapi.min.js', 'bootstrap/dist/css/bootstrap.css'],
            dest: 'dist/bower_components'
          }, {
            expand: true,
            dot: true,
            cwd: 'src',
            src: ['fonts/*', 'images/*', 'scripts/*', 'styles/app.css', 'views/*', 'index.html'],
            dest: 'dist'
          }]
        }
      },
      replace: {
        manifest_develop: {
          src: ['manifest.template.json'],
          dest: 'manifest.json',
          replacements: [{
            from: '%BUILD_PATH%',
            to: 'src/'
          }]
        },
        manifest_build: {
          src: ['manifest.template.json'],
          dest: 'dist/manifest.json',
          replacements: [{
            from: '%BUILD_PATH%',
            to: ''
          }]
        }
      },
      browserSync: {
        default_options: {
          bsFiles: {
            src: [
              "src/styles/*.css",
              "src/scripts/*.js",
              "src/views/*.html",
              "src/index.html"
            ]
          },
          options: {
            server: {
              baseDir: ["./", "./src"]
            },
            watchTask: true
          }
        }
      },
      watch: {
        sass: {
          files: ["src/styles/app.scss"],
          tasks: ['sass']
        }
      }
    });

    grunt.registerTask('build', ['sass', 'clean:dist', 'replace:manifest_build', 'copy:dist']);
    grunt.registerTask('build_local', ['replace:manifest_develop', 'sass']);

    grunt.registerTask('starwars', ['build_local', 'browserSync', 'watch']);

    return grunt.registerTask('default', ['starwars']);
  }

}).call(this);

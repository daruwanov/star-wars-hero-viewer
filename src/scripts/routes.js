(function () {
  'use strict';

  angular
    .module('starwarsapp')
    .config(routesFn);
  routesFn.$inject = ['$routeProvider'];

  function routesFn($routeProvider) {
    return $routeProvider
      .when('/', {
        templateUrl: 'views/people-list.html',
        controller: "PeopleListCtrl",
        resolve: {
          peopleList: ['swapi', function (swapi) {
            return swapi.people.page()
              .then(
                function (response) {
                  return {
                    data: response
                  };
                },
                function (response) {
                  //error
                }
              );
          }]
        }
      })
      .when('/people/:peopleId', {
        templateUrl: 'views/people-one.html',
        controller: "PeopleOneCtrl",
        resolve: {
          singlePerson: ['$route', 'swapi', '$q', 'peopleAvatars', function ($route, swapi, $q, peopleAvatars) {
            var person = {};
            return swapi.people.id($route.current.params.peopleId)
              .then(
                function (response) {
                  person = response;
                  var speciesPromises = _.map(person.species, function (_species) {
                    return swapi.get(_species);
                  });
                  return $q.all(speciesPromises);
                },
                function () {

                }
              )
              .then(
                function (response) {
                  var specArr = [];
                  _.forEach(response, function (spec) {
                    specArr.push(spec.name);
                  });
                  person._species = specArr.join();


                  var filmPromises = _.map(person.films, function (_films) {
                    return swapi.get(_films);
                  });
                  return $q.all(filmPromises);

                },
                function () {

                }
              )
              .then(
                function (response) {
                  var filmArr = [];
                  _.forEach(response, function (film) {
                    var filmId = film.url.split('/');
                    filmArr.push({title: film.title, id: filmId[filmId.length - 2]});
                  });
                  person._films = filmArr;

                  var vehiclePromises = _.map(person.vehicles, function (vehicle) {
                    return swapi.get(vehicle);
                  });
                  return $q.all(vehiclePromises);
                },
                function () {
                }
              ).then(
                function (response) {
                  var vehArr = [];
                  _.forEach(response, function (vehicle) {
                    var vehicleId = vehicle.url.split('/');
                    vehArr.push({name: vehicle.name, id: vehicleId[vehicleId.length - 2]});
                  });
                  person._vehicles = vehArr;

                  return peopleAvatars.getAvatar();
                },
                function () {
                }
              ).then(
                function (response) {
                  person.avatar = {
                    thumb: response.data.data['fixed_width_small_url'],
                    full: response.data.data['fixed_width_downsampled_url']
                  };

                  return person;
                },
                function () {
                }
              );
          }]
        }
      })
      .when('/film/:filmId', {
        templateUrl: 'views/people-list-in-resource.html',
        controller: "PeopleListCtrl",
        resolve: {
          peopleList: ['swapi', '$route', '$q', function (swapi, $route, $q) {
            var resourceName = "";
            return swapi.films.id($route.current.params.filmId)
              .then(
                function (response) {
                  resourceName = response.title;
                  var charactersPromises = _.map(response.characters, function (_character) {
                    return swapi.get(_character);
                  });
                  return $q.all(charactersPromises);
                },
                function () {
                }
              )
              .then(
                function (response) {
                  return {
                    data: response,
                    resourceTitle: resourceName,
                    resourceUrl: 'film/' + $route.current.params.filmId,
                  };
                },
                function () {
                }
              );

          }]
        }
      })
      .when('/vehicle/:vehiclId', {
        templateUrl: 'views/people-list-in-resource.html',
        controller: "PeopleListCtrl",
        resolve: {
          peopleList: ['swapi', '$route', '$q', function (swapi, $route, $q) {
            var resourceName = "";
            return swapi.vehicles.id($route.current.params.vehiclId)
              .then(
                function (response) {
                  resourceName = response.name;
                  var vehiclesPromises = _.map(response.pilots, function (_pilot) {
                    return swapi.get(_pilot);
                  });
                  return $q.all(vehiclesPromises);
                },
                function () {
                }
              )
              .then(
                function (response) {
                  return {
                    data: response,
                    resourceTitle: resourceName,
                    resourceUrl: 'vehicle/' + $route.current.params.vehiclId,
                  };
                },
                function () {
                }
              );

          }]
        }

      })
  }

}).call(this);

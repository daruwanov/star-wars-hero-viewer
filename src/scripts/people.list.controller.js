(function () {
  'use strict';

  angular
    .module('starwarsapp')
    .controller("PeopleListCtrl", PeopleListCtrlFn);
  PeopleListCtrlFn.$inject = ['$scope', 'peopleList'];
  function PeopleListCtrlFn($scope, peopleList) {
    $scope.peopleList = peopleList.data;
    $scope.currentPagePeopleList = [];
    $scope.resourceTitle = peopleList.resourceTitle;
    $scope.resourceUrl = peopleList.resourceUrl;
  }
}).call(this);

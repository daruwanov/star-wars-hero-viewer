(function () {
  'use strict';

  angular
    .module('starwarsapp')
    .directive("person", personFn);

  personFn.$inject = ['peopleAvatars'];

  function personFn(peopleAvatars) {
    var directive = {
      restrict: "E",
      template: "<div class='imgHolder'><a ng-href='#/people/{{ person.personId }}'><img class='img_thumb' ng-src='{{person.avatar.thumb}}'></a></div><div class='divider'></div><span class='link-wrapper'><a class='swtext' ng-href='#/people/{{ person.personId }}'>{{ person.name }}</a></span></div>",
      scope: {
        person: "="
      },
      link: link
    };

    return directive;

    function link(scope) {
      peopleAvatars.getAvatar(scope.person.name).then(
        function (response) {
          scope.person.avatar = {
            thumb: response.data.data['fixed_width_small_url'],
            full: response.data.data['fixed_width_downsampled_url']
          };
          var personId = scope.person.url.split('/');
          scope.person.personId = personId[personId.length - 2];
        },
        function () {
          //Oops...
        }
      );
    };

  }
}).call(this);

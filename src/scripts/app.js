(function () {
  'use strict';

  angular
    .module('starwarsapp', ['ngRoute', 'ne.swapi'])
    .config(configFunction);

  configFunction.$inject = ['$compileProvider'];

  function configFunction($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
  }

}).call(this);

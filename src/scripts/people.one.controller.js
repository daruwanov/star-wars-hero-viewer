(function () {
  'use strict';

  angular
    .module('starwarsapp')
    .controller("PeopleOneCtrl", PeopleOneCtrlFn);

  PeopleOneCtrlFn.$inject = ['$scope', 'singlePerson'];
  function PeopleOneCtrlFn($scope, singlePerson) {
    $scope.currentUser = singlePerson;
  }
}).call(this);

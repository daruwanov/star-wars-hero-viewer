(function () {
  'use strict';

  angular
    .module('starwarsapp')
    .factory('peopleAvatars', peopleAvatarsFn);

  peopleAvatarsFn.$inject = ['$http'];
  function peopleAvatarsFn($http) {
    function getAvatar(actorName) {
      return $http.get('http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=' + actorName);
    }

    return {
      getAvatar: getAvatar
    }
  }
}).call(this);

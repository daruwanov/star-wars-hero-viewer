(function () {
  'use strict';

  angular
    .module('starwarsapp')
    .directive("pagination", paginationFn);

  function paginationFn() {
    return {
      restrict: "E",
      templateUrl: "views/pagination.html",
      scope: {
        peopleList: "=",
        currentPagePeopleList: "=",
        source: "@"
      },
      controller: paginationCtrlFn
    };
  }

  paginationCtrlFn.$inject = ['$scope', 'swapi'];
  function paginationCtrlFn($scope, swapi) {
    $scope.currentPage = 1;
    $scope.getNumber = function (num) {
      return new Array(num);
    };
    $scope.getData = function (page) {

      if ($scope.source == 'remote') {
        swapi.people.page(page)
          .then(
            function (response) {
              $scope.currentPagePeopleList = _.chunk(response.results, 2);
              $scope.currentPage = page;
            },
            function (response) {
              //error
            }
          );
      } else {
        $scope.currentPage = page;
        $scope.currentPagePeopleList = _.chunk($scope.peopleList[$scope.currentPage - 1], 2);
      }
    };
    $scope.nextPage = function () {
      $scope.getData($scope.currentPage + 1);
    };
    $scope.prevPage = function () {
      $scope.getData($scope.currentPage - 1);
    };
    if ($scope.source == 'remote') {
      $scope.totalPages = parseInt($scope.peopleList.count / 10);
      if ($scope.peopleList.count % 10 != 0) {
        $scope.totalPages += 1;
      }
      $scope.currentPagePeopleList = _.chunk($scope.peopleList.results, 2);
    }
    else {
      $scope.peopleList = _.chunk($scope.peopleList, 10);
      $scope.totalPages = $scope.peopleList.length;
      $scope.currentPagePeopleList = _.chunk($scope.peopleList[$scope.currentPage - 1], 2);
    }
  }

}).call(this);
